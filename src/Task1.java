import java.util.Scanner;

public class Task1 {
    public static void task1() {
        //Verilmish metnde A dan ferqli nece simvol var indexof metodu ilede elemek olar.
        int count = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Xahish olunur metni ve ya sozu daxil: ");
        String metn = scanner.nextLine();
        int metnLength = metn.length();
        if (metnLength <= 1) {
            System.out.println("Metn ve ya soz uzunlugu 3 den cox olmalidir!");
            return;
        }
        System.out.println("Axtardiginiz simvolu daxil edin: ");
        String symbol = scanner.nextLine();
        if (symbol.length() == 0) {
            System.out.println("Simvol sayi 1 olmalidir");
            return;
        }
        for (int i = 0; i < metnLength - 1; i++) {
            char result = metn.charAt(i);
            if (result == symbol.charAt(0)) {
                count++;
            }

        }
        System.out.println("Daxil etdiyiniz metnin umumi uzunlugu: " + metnLength);
        System.out.println("Axtardiginiz simvol (" + symbol + ") sayi: " + count);
        System.out.println("Diger simvol saylari :" + (metnLength - count));

    }
}
