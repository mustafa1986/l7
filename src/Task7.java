import java.util.Scanner;

public class Task7 {
    public static void task7() {
        //Verilmish metnde reqemlerin sayin tapmali
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        if (metn.length() < 1 ) {
            System.out.println("Metn daxil edilmeyib");
            return;
        }
        for (int i = 0; i <= metn.length() - 1; i++) {
            char symbol = metn.charAt(i);
            if (Character.isDigit(symbol)) {
                count++;
            }
        }
        if (count==0){
            System.out.println("Daxil etdiyiniz metnde reqem yoxdur! ");
        }else {
            System.out.println("Daxil etdiyiniz metnde reqem sayi: " + count );
        }
    }

}
