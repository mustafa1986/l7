import java.util.Locale;
import java.util.Scanner;

public class Task4 {
    public static void task4() {
        //verilmish metnde ilk simvoldan bashqa butun herfler kicik olsun
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        metn = metn.toUpperCase();
        char firstSymbol = metn.charAt(0);
        System.out.println("Duzelishden sonra metn:\n " + firstSymbol + metn.substring(1, metn.length()).toLowerCase());

    }
}
