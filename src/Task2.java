import java.util.Scanner;

public class Task2 {
    public static void task2() {
        //Verilmish metnde son 15 simvolun daxilinde "B" simvolu varmi
        int symbolCount = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        int metnLength = metn.length();
        if (metnLength < 15) {
            System.out.println("Metn minimum 15 ve daha yuxari  simvol  olmalidir! ");
            return;
        }

        System.out.println("Axtardiginiz simvolu daxil edin: ");
        String symbol = scanner.nextLine();
        if (symbol.length() <= 0) {
            System.out.println("Simvol sayi 1 olmalidir");
            return;
        }
        for (int i = metnLength - 15; i < metnLength - 1; i++) {
            char result = metn.charAt(i);
            if (result == symbol.charAt(0)) {
                symbolCount++;
            }

        }
        System.out.println("Daxil etdiyiniz metnin umumi uzunlugu: " + metnLength);
        System.out.println("Axtardiginiz simvol (" + symbol + ") sayi: " + symbolCount);
    }


}
