import java.util.Locale;
import java.util.Scanner;

public class Task5 {
    public static void task5() {
        //verilmish metnde son simvoldan bashqa diger simvollarin hamisin boyukle cap etmeli
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        metn=metn.toLowerCase();
        char lastSymbol= metn.charAt(metn.length()-1);
        System.out.println("Duzelishden sonra metn:\n "+ metn.substring(0,metn.length()-1).toUpperCase()+lastSymbol);
    }

}
