import java.util.Scanner;

public class Task6 {
    public static void task6() {
        // Verilmish metnde butun A silvollarini ilk B simvollarini ise son simvolla evez et
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        if (metn.length() < 2) {
            System.out.println("Metn 2 simvoldan boyuk olmalidir");
            return;
        }
        char firstSymbol = metn.charAt(0);
        char lastSymbol= metn.charAt(metn.length()-1);
        metn=metn.replace("A",Character.toString(firstSymbol));
        metn=metn.replace("B",Character.toString(lastSymbol));
        System.out.println("Duzelishden sonra metn: \n"+ metn);
    }
}
