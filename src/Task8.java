import java.util.Scanner;

public class Task8 {
    public static void task8(){

        // Verilmish metnde reqemlerden bashqa diger simvollarin sayin tapmali
        Scanner scanner = new Scanner(System.in);
        int countNumber = 0;
        int countSymbol = 0;
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        if (metn.length() < 1 ) {
            System.out.println("Metn daxil edilmeyib");
            return;
        }
        for (int i = 0; i <= metn.length() - 1; i++) {
            char symbol = metn.charAt(i);
            if (Character.isDigit(symbol)) {
                countNumber++;
            }else {
                countSymbol++;
            }
        }
        System.out.println("Daxil edilmish metnin uzunlugu: "+ metn.length());
        System.out.println("Daxil edilmish metnde reqem sayi: " + countNumber);
        System.out.println("Daxil edilmish metnde reqemlerden gerli simvol sayi: "+ countSymbol);
    }
}
